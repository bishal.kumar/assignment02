# Description

## Screenshots

# Checklist:

- [ ] All the files are formatted with a preferred formatter.
- [ ] No unnecessary comments.
- [ ] No debugger statements.
- [ ] The build is working fine with the latest changes.
- [ ] No errors related to accessibility, SEO, and Best Practices in the Lighthouse audit.
- [ ] I have added proper documentation to my code wherever needed.
- [ ] Changes to the reusable part of my code doesn't break anything.
- [ ] I have done a self-review.
