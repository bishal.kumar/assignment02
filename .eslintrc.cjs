module.exports = {
	env: {
		browser: true,
		es2021: true,
	},
	extends: ['plugin:react/recommended', 'airbnb', 'prettier'],
	overrides: [],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 'latest',
		sourceType: 'module',
	},
	plugins: ['react'],
	// Enforces a convention in the order of import statements.
	rules: {
		'react/react-in-jsx-scope': 0,
		'import/order': [
			'error',
			{
				groups: [
					'builtin',
					'external',
					'internal',
					'parent',
					'sibling',
					'index',
				],
				'newlines-between': 'always',
				pathGroups: [
					{
						pattern: '@src/**',
						group: 'internal',
					},
					{
						pattern: '@components/**',
						group: 'internal',
					},
					{
						pattern: '@common/**',
						group: 'internal',
					},
					{
						pattern: '@pages/**',
						group: 'internal',
					},
					{
						pattern: '@redux/**',
						group: 'internal',
					},
					{
						pattern: '@sections/**',
						group: 'internal',
					},
					{
						pattern: '@styles/**',
						group: 'internal',
					},
					{
						pattern: '@assets/**',
						group: 'internal',
						position: 'after',
					},
				],
				// By default, in the context of a particular pathGroup entry, when setting position, a new "group" will silently be created. This removes this unwanted behavior.
				distinctGroup: false,
				// This defines import types that are not handled by configured pathGroups. This is mostly needed when you want to handle path groups that look like external imports.
				pathGroupsExcludedImportTypes: ['internal'],
				alphabetize: {
					order: 'asc',
					caseInsensitive: true,
				},
			},
		],
	},
	// referencing resolvers for all folders inside src.
	settings: {
		'import/resolver': {
			alias: {
				map: [
					['@src', './src'],
					['@assets', './src/assets'],
					['@common', './src/common'],
					['@components', './src/components'],
					['@pages', './src/pages'],
					['@redux', './src/redux'],
					['@sections', './src/sections'],
					['@styles', './src/styles'],
				],
				extensions: ['.js', '.jsx', '.css'],
			},
		},
	},
};
