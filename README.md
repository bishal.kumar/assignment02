# Assignment 02

## Technologies Used

- ReactJs: used for building user interfaces.
- Redux: used for managing and centralizing application state.
- Prettier: used for Code Formatting.
- Eslint: used as a linter.
- MUI: used as react component library.
- Vite: used as a bundler.

## Project Folder Structure

# src

- [public/](./react/assignment02/public)
- [src/](./react/assignment02/src)
  - [assets/](./react/assignment02/src/assets)
    - [icons/](./react/assignment02/src/assets/icons)
    - [images/](./react/assignment02/src/assets/images)
      - [large-screens/](./react/assignment02/src/assets/images/large-screens)
      - [small-screens/](./react/assignment02/src/assets/images/small-screens)
    - [typography/](./react/assignment02/src/assets/typography)
      - [Inter-Bold/](./react/assignment02/src/assets/typography/Inter-Bold)
      - [Inter-Medium/](./react/assignment02/src/assets/typography/Inter-Medium)
      - [Inter-Regular/](./react/assignment02/src/assets/typography/Inter-Regular)
      - [Inter-SemiBold/](./react/assignment02/src/assets/typography/Inter-SemiBold)
  - [common/](./react/assignment02/src/common)
    - [constants/](./react/assignment02/src/common/constants)
    - [utils/](./react/assignment02/src/common/utils)
  - [components/](./react/assignment02/src/components)
    - [icon/](./react/assignment02/src/components/icon)
  - [pages/](./react/assignment02/src/pages)
    - [NotFound/](./react/assignment02/src/pages/NotFound)
    - [Overview/](./react/assignment02/src/pages/Overview)
  - [redux/](./react/assignment02/src/redux)
  - [styles/](./react/assignment02/src/styles)
    - [theme/](./react/assignment02/src/styles/theme)
  - [app.jsx](./react/assignment02/src/app.jsx)
  - [main.jsx](./react/assignment02/src/main.jsx)
  - [mockData.json](./react/assignment02/src/mockData.json)
- [index.html](./react/assignment02/index.html)

## Setup/Installation Requirements

- _Clone the project here: [Gitlab link](https://code.jtg.tools/bishal.kumar/assignment02/-/tree/dev)_
- _Inside Assignment directory, Run: `nvm use`. "Switches to required version of node (if available)"._
- _Run: `nvm install`. "if required node version is not available"._
- _Run: `yarn`. "to install all the dependencies."_

### 1. Linting and formatting

- _Run: `yarn format`. "to format all files supported by Prettier in the root directory and its subdirectories."_
- _Run: `yarn lint`. "to run eslint on the root directory and instruct ESLint to try to fix as many issues as possible."._

### 2. Running and Building of project

- _Run `yarn build`. "to build the production build."_
- _Run `yarn start`. "to run the project using webpack Dev Server."_
- _Run `yarn start:prod`. "build the production build and preview."_

### 3. Deploying production build on netlify

<!-- TODO will be added in next task -->

## Link for deployed web app

<!-- TODO will be added in next task -->

## Screenshots

<!-- TODO will be added in next task -->
