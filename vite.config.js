/* eslint-disable import/no-extraneous-dependencies */
import * as path from 'path';

import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [react()],
	resolve: {
		alias: [
			{
				find: '@src',
				replacement: path.resolve(__dirname, 'src'),
			},
			{
				find: '@assets',
				replacement: path.resolve(__dirname, 'src/assets'),
			},
			{ find: '@common', replacement: path.resolve(__dirname, 'src/common') },
			{
				find: '@components',
				replacement: path.resolve(__dirname, 'src/components'),
			},
			{ find: '@pages', replacement: path.resolve(__dirname, 'src/pages') },
			{ find: '@redux', replacement: path.resolve(__dirname, 'src/redux') },
			{
				find: '@sections',
				replacement: path.resolve(__dirname, 'src/sections'),
			},
			{ find: '@styles', replacement: path.resolve(__dirname, 'src/styles') },
		],
	},
	build: {
		rollupOptions: {
			output: {
				assetFileNames: (assetInfo) => {
					let extType = assetInfo.name.split('.').at(1);
					if (/webp/i.test(extType)) {
						extType = 'images';
					}
					return `assets/${extType}/[name]-[hash][extname]`;
				},
			},
		},
	},
});
