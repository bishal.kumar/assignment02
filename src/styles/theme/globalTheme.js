import { createTheme } from '@mui/material/styles';

import colors from '@common/constants';
import font from '@styles/font';
import icons from '@styles/icons';

const theme = createTheme({
	palette: {
		primary: {
			main: colors.grey[100],
			dark: colors.grey[200],
		},
		success: {
			main: colors.green[100],
			dark: colors.green[200],
			contrastText: colors.green[300],
		},
		error: {
			main: colors.red[100],
			contrastText: colors.red[200],
		},
		info: {
			main: colors.blue[100],
			contrastText: colors.blue[200],
		},
		// color variants for lines
		grey: {
			light: colors.grey[400],
			dark: colors.grey[300],
		},
		// color variants for texts
		text: {
			primary: colors.grey[600],
			secondary: colors.grey[500],
			disabled: colors.grey[100],
		},
	},
	typography: {
		htmlFontSize: 10,
		fontFamily: 'Inter, sans-serif',
		h1: {
			fontWeight: 700,
			fontSize: '3rem',
			lineHeight: '4.5rem',
		},
		h2: {
			fontWeight: 700,
			fontSize: '2rem',
			lineHeight: '3rem',
		},
		h3: {
			fontWeight: 600,
			fontSize: '1.6rem',
			lineHeight: '2.4rem',
		},
		h4: {
			fontWeight: 600,
			fontSize: '2rem',
			lineHeight: '3rem',
		},
		h5: {
			fontWeight: 600,
			fontSize: '1.2rem',
			lineHeight: '1.8rem',
		},
		h6: {
			fontWeight: 500,
			fontSize: '1.6rem',
			lineHeight: '2.4rem',
		},
		subtitle: {
			fontWeight: 500,
			fontSize: '1.2rem',
			lineHeight: '1.8rem',
		},
		body1: {
			fontWeight: 400,
			fontSize: '1.6rem',
			lineHeight: '2.4rem',
		},
		body2: {
			fontWeight: 400,
			fontSize: '1.2rem',
			lineHeight: '1.8rem',
		},
		button: {
			fontWeight: 600,
			fontSize: '1.4rem',
			lineHeight: '2.1rem',
		},
		caption: {
			fontWeight: 400,
			fontSize: '1.4rem',
			lineHeight: '2rem',
		},
	},
	components: {
		MuiCssBaseline: {
			styleOverrides: `${font} ${icons}`,
		},
	},
});

export default theme;
