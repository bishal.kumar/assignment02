import icomoonIcons from '@assets/icons/icomoon.woff';

const icons = `
@font-face {
  font-family: 'icomoon';
  src:url(${icomoonIcons}) format('woff');
  font-weight: normal;
  font-style: normal;
  font-display: block;
}

[class^="icon-"], [class*=" icon-"] {
  font-family: 'icomoon' !important;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;

  /* Better Font Rendering =========== */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.icon-exclamation-circle1:before {
  content: "\\e914";
}
.icon-exclamation-circle:before {
  content: "\\e913";
}
.icon-adjustments:before {
  content: "\\e909";
}
.icon-cog:before {
  content: "\\e90c";
}
.icon-globe:before {
  content: "\\e910";
}
.icon-search:before {
  content: "\\e900";
  color: #6b7280;
}
.icon-notification:before {
  content: "\\e901";
}
.icon-twitter:before {
  content: "\\e902";
}
.icon-support:before {
  content: "\\e903";
}
.icon-shopping-bag:before {
  content: "\\e904";
}
.icon-menu:before {
  content: "\\e905";
}
.icon-Logo:before {
  content: "\\e906";
}
.icon-lock-closed:before {
  content: "\\e907";
}
.icon-inbox:before {
  content: "\\e908";
}
.icon-github:before {
  content: "\\e90a";
}
.icon-facebook:before {
  content: "\\e90b";
}
.icon-dribbble:before {
  content: "\\e90d";
}
.icon-document:before {
  content: "\\e90e";
}
.icon-collection:before {
  content: "\\e90f";
}
.icon-clipboard:before {
  content: "\\e911";
}
.icon-chart-pie:before {
  content: "\\e912";
  color: #0e9f6e;
}
`;

export default icons;
