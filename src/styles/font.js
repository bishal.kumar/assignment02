import InterBoldWoff from '@assets/typography/Inter-Bold/Inter-Bold.woff';
import InterBoldWoff2 from '@assets/typography/Inter-Bold/Inter-Bold.woff2';
import InterMediumWoff from '@assets/typography/Inter-Medium/Inter-Medium.woff';
import InterMediumWoff2 from '@assets/typography/Inter-Medium/Inter-Medium.woff2';
import InterRegularWoff from '@assets/typography/Inter-Regular/Inter-Regular.woff';
import InterRegularWoff2 from '@assets/typography/Inter-Regular/Inter-Regular.woff2';
import InterSemiBoldWoff from '@assets/typography/Inter-SemiBold/Inter-SemiBold.woff';
import InterSemiBoldWoff2 from '@assets/typography/Inter-SemiBold/Inter-SemiBold.woff2';

const font = `
@font-face {
  font-family: 'Inter';
  src: url(${InterRegularWoff2}) format('woff2'),
    url(${InterRegularWoff}) format('woff');
  font-weight: 400;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: 'Inter';
  src: url(${InterMediumWoff2}) format('woff2'),
    url(${InterMediumWoff}) format('woff');
  font-weight: 500;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: 'Inter';
  src: url(${InterSemiBoldWoff2}) format('woff2'),
    url(${InterSemiBoldWoff}) format('woff');
  font-weight: 600;
  font-style: normal;
  font-display: swap;
}

@font-face {
  font-family: 'Inter';
  src: url(${InterBoldWoff2}) format('woff2'),
    url(${InterBoldWoff}) format('woff');
  font-weight: 700;
  font-style: normal;
  font-display: swap;
}`;

export default font;
