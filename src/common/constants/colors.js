const colors = {
	green: { 100: '#DEF7EC', 200: '#0E9F6E', 300: '#03543F' },
	red: { 100: '#FBD5D5', 200: '#9B1C1C' },
	blue: { 100: '#E1EFFE', 200: '#1E429F' },
	grey: {
		100: '#FFFFFF',
		200: '#F9FAFB',
		300: '#F3F4F6',
		400: '#E5E7EB',
		500: '#6B7280',
		600: '#111827',
	},
};

export default colors;
