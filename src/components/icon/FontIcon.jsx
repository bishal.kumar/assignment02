import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

// Create a Icon component that'll render an <span> tag with some styles
const StyledIcon = styled.span`
	font-size: ${(props) => props.iconSize};

	/**
* @summary Takes prop from user and returns a styled span component.
* @param {*} props - Takes iconClass(class name of the icon) and iconSize(required size of the icon).
* @return {styled component} returns a styled component(StyledIcon) for icons with passed properties.
*/
`;
function fontIcon(props) {
	const FontClass = props.iconClass;
	const FontSize = props.iconSize;

	return <StyledIcon className={FontClass} iconSize={FontSize} />;
}

// Declaring that a prop is a specific JS primitive.
fontIcon.propTypes = {
	iconSize: PropTypes.string,
	iconClass: PropTypes.string.isRequired,
};

// Gets applied if no prop is passed by user
fontIcon.defaultProps = {
	iconSize: '2rem',
};

export default fontIcon;
