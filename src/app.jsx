import { ThemeProvider } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';

import Overview from '@pages/Overview';
import globalTheme from '@styles/theme/globalTheme';

export default function App() {
	return (
		<ThemeProvider theme={globalTheme}>
			<CssBaseline />
			<Overview />
		</ThemeProvider>
	);
}
