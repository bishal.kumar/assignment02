import Typography from '@mui/material/Typography';

export default function NotFound() {
	return (
		<div className="wrapper">
			<Typography variant="h1" color="text.secondary">
				Error 404
			</Typography>
		</div>
	);
}
