import { createStore } from 'redux';

import mockData from '@src/mockData.json';

const store = createStore(() => ({
	data: mockData,
}));

export default store;
